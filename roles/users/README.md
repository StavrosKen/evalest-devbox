# Users
This role manages the system users and ssh access. All EH users have "webdev" as their main group and have access to SUDO. File permissions are overridden by ACL using the "webdev" group, so that any user can upload a website to /var/www/. All users are created with a random, unknown password, as they should not need it.

SSH access only allows the usage of key - no user/password. 

## SSH Keys
Keys are stored in /files. A reference to the key should be provided in the config vars
