#!/bin/bash

if [ ! -d "/var/mongodump/" ]; then
  mkdir /var/mongodump/
fi

mongodump --out /var/mongodump/