#Evalest Devbox#

Creates a Linux virtual machine with all the necessary tools needed for fullstack development installed

### Contains ###

* Ubuntu 20 Bionic
* NGINX
* PHP
* NVM
* MongoDB, PostgeSQL
* CRON and cron job management
* Project folder structure creation
* SystemD services
* SSL certificate uploads
* Users ssh keys

### Prerequisutes ###

* Oracle VirtualBox
* Vagrant

On Windows, these Windows features must be off:

* Containers
* Hyper-V
* Virtual Machine Platform
* Windows Hypervisor Platform
* Windows Sandbox
* Windows Subsystem for Linux (WSL)

### How to install ###

* Clone the repo
* Run `vagrant up` from your terminal

### Changes to Ansible ###

Whenever you make changes to Ansible provisioning, you must run `vagrant provision`.

### How do I access my devbox? ###

* Run vagrant ssh from your terminal to access via ssh, or
* Open your virtualbox client, select Evalest-Devbox and press Show to access the GUI