#Evalest Devbox#

### Contains ###

* Linux Mint
* Git
* Node
* MongoDB
* Sublime 3
* Google Chrome
* GitKraken
* Postman

### How to install ###

* Clone the repo
* Run vagrant up from your terminal
* Run vagrant provision from your terminal

### How do I access my devbox? ###

* Run vagrant ssh from your terminal to access via ssh, or
* Open your virtualbox client, select Evalest-Devbox and press Show to acess the